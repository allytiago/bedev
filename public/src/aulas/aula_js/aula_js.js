console.log("Hello World")

// Variaveis
// Espaço reservado na memória
// O = é simbolo de atribuição (recebe)
// Quando o javascript trava num erro, ele não lê as linhas abaixo

// let = variável que varia / mutável
let fruta = "Maçã"

fruta = "Morango" //redeclarou a variável

// const = variável constante / imutável / não tem como redeclarar
const docePreferidoMaria = "Sorvete de Chiclete"
console.log(docePreferidoMaria)


// String é tudo que está dentro das aspas simples ou duplas
// String é texto

let diaDeHoje = "27 de julho de 2022"

// Number = numero
let dia = 27

// Booleano = dado lógico (true ou falso)
let serProgramadorEm2022 = true
let churrosEhRuim = false

// array = tipo de dados, pode colocar booleans, numeros, strings, objetos...
let listaCompras = ["maçã", "alface", "sorvete de chiclete"]
let listaAleatoria = [1, true, "oi", [1,3,4]]

//No javascript, as posições começão a partir do número 0 e não a partir do número 1
// no array, a ordem númerica começa do 0
console.log(listaAleatoria[0])
console.log(listaCompras[2])

//.length = para saber quantos elementos tem no array
console.log(listaCompras.length)

//.push() = para adicionar elementos em um array
listaCompras.push("chocolate")

console.log(listaCompras)

// Objeto é uma coleção de dados identificados por uma chave
// Dentro de um objeto pode haver string, number, array, boolean...
let bolo = {
    sabor: "Chocolate",
    recheio: "Leite Ninho",
    cobertura: "Brigadeiro",
    preco: 20,
    gostoso: true,
}
console.log(bolo)

//Para acessar a chave espessífica do objeto, colocar o nome da chave depois da variável e unir com um ponto
console.log(bolo.sabor)

// Para adicionar uma nova chave
// bolo['nome da chave'] = valor
bolo['confeitaria'] = "Doce muito bom"

console.log(bolo)

// Operadores lógicos
//=== estritamente igual => verifica se os valores são iguais e do mesmo tipo
let a = 10
let b = "10"
let c = 20
let d = 10

console.log(a === b)//false
console.log(a === d)//true

// == igualdade => se os valores são iguais independentemente do tipo
console.log(a == b)//true

// !== não identico => avalia se os valores e os tipos são diferentes. O oposto do estritamente igual
console.log(a !== b)//true
console.log(a !== d)//true

// != diferente => avalia se os valores são diferentes, independente do tipo, avalia só o valor
console.log(a != c)//true

// < menor que
console.log(a<c)//true

// > maior que
console.log(a>c)//false

// <= menor ou igual
console.log(a<=c)//true

// >= maior ou igual
console.log(a<=c)//true

/* 
* && => Ê / comparação
* true  && true  => true
* true  && false => false
* false && true  => false
* false && false => false
*/
console.log(a === b && a === d)//false => um deles é false
console.log(a == b && a === d)//true => os dois retornaram true

/* 
* || => Ou
* true  || true  => true
* true  || false => true
* false || true  => true
* false || false => true
*/


/*
* O && é rigoroso, só é verdade se tudo verdade
* Já o || é tudo de boa, só é falso se for tudo falso
*
* By aluno PJ
*/

/* Estrutura condicional
* "e se"
*
* se chover
* vou ver série
*
* se não chover
* vou pra praia
*/

let chuva = false
let neve = true
/* if (condição){
    código a ser executado
}else if (condição){
    código a ser executado
} else (condição){
    código a ser executado
}
*/
if (chuva === true){
    console.log("Vou ver minha série")
} else if (neve === true) {
    console.log("Vou tomar chocolate quente!")
} else {
    console.log("Vou pra praia")
}

// Função
// São blocos de código que executa determinadas funções e tudo que é executado dentro daquele bloco, fica naquele bloco a menos que ele renorne alguma coisa

/* 
function nomeDaFuncao (parâmetros){
    seu código
}
=> os parâmetros são informações externas que são precisas para que o código funcione.
=> Os parâmetros são opicionais, pois pode ser que dentro do bloco, já tenha tudo que precisa. Depende da sua lógica
*/

//OBS.: toda vez que criar variáveis, objetos, funções  pense em nomes intuitivos para que caso um desenvolvedor externo vá resolver algo, possa entender o código

function soma(numberOne, numberTwo){
    // console.log(numberOne + numberTwo)
    return numberOne + numberTwo //rertorna a soma das variáveis caso seja solicitado externamente
}

// se o console.log estiver imprimindo o valor dentro da function, não precisa chamar novamente fora
// soma(27, 59)
// soma(1359, 1473)

let resultadoSoma = soma(1359, 1473)
console.log(resultadoSoma) //undefined

function saudacao(){
    return "Oi! Tudo bem? como vai?"
}

console.log(saudacao())//a função está só retornando um valor e não imprimindo, por isso tem que chamar o console.log

// Estrutura de repetição/loops => serve para executar o mesmo código várias vezes

/*
for (inicialização; condição; incremento){
    código a ser executado
}
=> primeiro inicializa o loop 
- define uma váriavel antes do inicio do loop, uma variável contadora, que faz uma contagem

=> segundo, a condição para que o loop possa prosseguir ou parar
- define uma condição para executar o loop

=> o terçeiro incrementa
- ele pode incrementar ou decrementar um valor a cada vez que o código executar
*/

let modulos = ["M1", "M2", "M3", "M4", "M5", "M6"]
// console.log(modulos[0]) //chamando a posição do array
// console.log(modulos[1]) 
// console.log(modulos[2])
// console.log(modulos[3])
// console.log(modulos[4])
// console.log(modulos[5])

// o for loop vai percorrer/interar o array e vai imprimir no console cada um dos valores da variável "modulo"

// ++ => incremento

// Na sua lóica, você pode iniciar o seu contador em qualquer posição você quiser
for (let contadora = 0; contadora < modulos.length; contadora++ ){ // enquanto a minha variável "contadora" for menor que o "modulos" então, vai incrementar mais 1
    console.log(modulos[contadora])
    // a variável contadora começou na posição 0
    // a variável modulos vai até a 5 posição
    // se 0 for menor que a quantidade de elementos/tamanho da variável "modulos" então vai incrementar mais 1
}

// é comum que a variável "i" siginifica index
for (let i = 0; i < 5; i++){
    if(i % 2 === 0){
        console.log("par" + i)
    } else {
        console.log("impar" + i)
    }
}

// o poerador módulo (%) faz a divisão entre dois numeros e retorna o resto